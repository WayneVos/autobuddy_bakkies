(function () {
    "use strict";
    angular.module("app")
       .directive('convertToNumber', function () {
           return {
               require: 'ngModel',
               link: function (scope, element, attrs, ngModel) {
                   ngModel.$parsers.push(function (val) {
                       return val != null ? parseInt(val, 10) : null;
                   });
                   ngModel.$formatters.push(function (val) {
                       return val != null ? '' + val : null;
                   });
               }
           }
       })
       .directive('focusOn', function () {
           return function (scope, elem, attr) {
               scope.$on('focusOn', function (e, name) {
                   if (name === attr.focusOn) {
                       elem[0].focus();
                   }
               });
           };
       })
       .factory('focus', function ($rootScope, $timeout) {
           return function (name) {
               $timeout(function () {
                   $rootScope.$broadcast('focusOn', name);
               });
           }
       })
       .controller("showroom", ["$scope", "$http", "$state", "focus", function ($scope, $http, $state, focus) {
           var vm = this;
           var stateParams = $state.params;
           vm.data = [];
           vm.selectData = [];
           vm.detailsdata = {};
           vm.makeData = [];
           vm.seriesData = [];
           vm.colourData = [];
           vm.yearData = [];
           vm.bodyData = [];
           $scope.selectMake = [];
           $scope.selectSeries = [];
           $scope.selectColour = [];
           $scope.selectYear = [];
           $scope.selectBody = [];
           $scope.selectedMinPrice = [];
           $scope.selectedMaxPrice = [];
           $scope.selectedMinPrime = [];
           $scope.selectedMaxPrime = [];
           $scope.selectMinMile = [];
           $scope.selectMaxMile = [];
           $scope.limitSet = 12;
           $scope.perPage = 12;
           var companyIdFilter = "company_id=eq.557";
           var dataUrl = "http://autobuddy.vmgtest.co.za/service_api/view_stock_complete?" + companyIdFilter;

           function getSelectedMake() {
               return $scope.selectMake ? $scope.selectMake.make : "";
           }

           function getSelectedSeries() {
               return $scope.selectSeries ? $scope.selectSeries.series : "";
           }

           function getSelectedMinPrice() {
               return stateParams.minP ? stateParams.minP : "";
           }

           function getSelectedMaxPrice() {
               return stateParams.maxP ? stateParams.maxP : "";
           }

           function getSelectedMinPrime() {
               return stateParams.minPrime ? stateParams.minPrime : "";
           }

           function getSelectedMaxPrime() {
               return stateParams.maxPrime ? stateParams.maxPrime : "";
           }

           function getSelectedMinMile() {
               return $scope.selectMinMile ? $scope.selectMinMile : "";
           }

           function getSelectedMaxMile() {
               return $scope.selectMaxMile ? $scope.selectMaxMile : "";
           }

           function getSelectedYear() {
               return $scope.selectYear ? $scope.selectYear.year : "";
           }

           function getSelectedColour() {
               return $scope.selectColour ? $scope.selectColour.colour : "";
           }

           function getSelectedBody() {
               return $scope.selectBody ? $scope.selectBody.body_type : "";
           }

           function fetchData() {
               var filterState = (getSelectedMake() !== "" ? "&make=eq." + getSelectedMake() : "") +
                  (getSelectedSeries() !== "" ? "&series=eq." + getSelectedSeries() : "") +
                  (getSelectedYear() !== "" ? "&year=eq." + getSelectedYear() : "") +
                  (getSelectedColour() !== "" ? "&colour=eq." + getSelectedColour() : "") +
                  (getSelectedBody() !== "" ? "&body_type=eq." + getSelectedBody() : "") +
                  (getSelectedMinPrice() !== "" ? "&selling_price=gte." + getSelectedMinPrice() : "") +
                  (getSelectedMaxPrice() !== "" ? "&selling_price=lte." + getSelectedMaxPrice() : "") +
                  (getSelectedMinPrime() !== "" ? "&premium=gte." + getSelectedMinPrime() : "") +
                  (getSelectedMaxPrime() !== "" ? "&premium=lte." + getSelectedMaxPrime() : "") +
                  (getSelectedMinMile() !== "" ? "&mileage=gte." + getSelectedMinMile() : "") +
                  (getSelectedMaxMile() !== "" ? "&mileage=lte." + getSelectedMaxMile() : "");
               $http.get(dataUrl + filterState).then(function (response) {
                   vm.data = response.data;
                   $scope.totalCars = vm.data.length;
               })
           }

           function initLoad() {
               $scope.selectMake = stateParams.make;
               $scope.selectSeries = stateParams.series;
               $scope.selectColour = stateParams.colour;
               $scope.selectBody = stateParams.body_type;
               $scope.selectMinPrice = stateParams.minP;
               $scope.selectMaxPrice = stateParams.maxP;
               $scope.selectMinMile = stateParams.minM;
               $scope.selectMaxMile = stateParams.maxM;
               $http.get(dataUrl + "&limit=" + $scope.limitSet + selectFilter()).then(function (response) {
                   vm.data = response.data;
               });
           }

           function loadData() {
               $http.get(dataUrl + "&select=make,series,year,colour,body_type,year" + selectFilter()).then(function (response) {
                   vm.makeData = [];
                   vm.seriesData = [];
                   vm.colourData = [];
                   vm.yearData = [];
                   vm.bodyData = [];
                   for (var i = 0; i < response.data.length; i++) {
                       vm.makeData.push(response.data[i]);
                       vm.seriesData.push(response.data[i]);
                       vm.colourData.push(response.data[i]);
                       vm.yearData.push(response.data[i]);
                       vm.bodyData.push(response.data[i]);
                   }
               });
           }

           function selectFilter() {
               var filterUrl = '';
               $scope.pricesearch = 0;
               if (stateParams.make !== null && stateParams.make !== undefined) {
                   filterUrl += "&make=eq." + stateParams.make;
                   var make = {make: $state.params.make};
                   vm.makeData.push(make);
                   $scope.selectMake = vm.makeData[0];
                   $scope.limitSet = "All";
               }
               if (stateParams.series !== null && stateParams.series !== undefined) {
                   filterUrl += "&series=eq." + stateParams.series;
                   var series = {series: $state.params.series};
                   vm.seriesData.push(series);
                   $scope.selectSeries = vm.seriesData[0];
                   $scope.limitSet = "All";
               }
               if (stateParams.year !== null && stateParams.year !== undefined) {
                   filterUrl += "&year=eq." + stateParams.year;
                   var year = {year: $state.params.year};
                   vm.yearData.push(year);
                   $scope.selectYear = vm.yearData[0];
                   $scope.limitSet = "All";
               }
               if (stateParams.colour !== null && stateParams.colour !== undefined) {
                   filterUrl += "&colour=eq." + stateParams.colour;
                   var colour = {colour: stateParams.colour};
                   vm.colourData.push(colour);
                   $scope.selectColour = vm.colourData[0];
                   $scope.limitSet = "All";
               }
               if (stateParams.body_type !== null && stateParams.body_type !== undefined) {
                   filterUrl += "&body_type=eq." + stateParams.body_type;
                   var body = {body_type: $state.params.body_type};
                   vm.bodyData.push(body);
                   $scope.selectBody = vm.bodyData[0];
                   $scope.limitSet = "All";
               }
               if (stateParams.minP || stateParams.maxP) {
                   $scope.pricesearch == '0';
                   $scope.selectedMinPrice[0] = stateParams.minP;
                   $scope.selectedMaxPrice[0] = stateParams.maxP;
               }
               if (stateParams.minPrime || stateParams.maxPrime) {
                   $scope.pricesearch == '1';
                   $scope.selectedMinPrime[0] = stateParams.minPrime;
                   $scope.selectedMaxPrime[0] = stateParams.maxPrime;
               }
               if (stateParams.minP !== null && stateParams.minP !== undefined) {
                   $scope.pricesearch = 0;
                   filterUrl += "&selling_price=gte." + stateParams.minP;
                   $scope.selectedMinPrice[0] = stateParams.minP;
               }
               if (stateParams.maxP !== null && stateParams.maxP !== undefined) {
                   $scope.pricesearch = 0;
                   filterUrl += "&selling_price=lte." + stateParams.maxP;
                   $scope.selectedMaxPrice[0] = stateParams.maxP;
               }
               if (stateParams.minPrime !== null && stateParams.minPrime !== undefined) {
                   $scope.pricesearch = 1;
                   filterUrl += "&premium=gte." + stateParams.minPrime;
                   $scope.selectedMinPrime[0] = stateParams.minPrime;
               }
               if (stateParams.maxPrime !== null && stateParams.maxPrime !== undefined) {
                   $scope.pricesearch = 1;
                   filterUrl += "&premium=lte." + stateParams.maxPrime;
                   $scope.selectedMaxPrime[0] = stateParams.maxPrime;
               }
               if (stateParams.minM !== null && stateParams.minM !== undefined) {
                   filterUrl += "&mileage=gte." + stateParams.minM;
                   $scope.selectMinMile = stateParams.minM;
                   $scope.limitSet = "All";
               }
               if (stateParams.maxM !== null && stateParams.maxM !== undefined) {
                   filterUrl += "&mileage=lte." + stateParams.maxM;
                   $scope.selectMaxMile = stateParams.maxM;
                   $scope.limitSet = "All";
               }
               return filterUrl;
           }

           $scope.focus = function (car) {
               vm.detailsdata = car;
               $state.params.car = car;
               $state.go('cardetails', {
                   make: car.make,
                   series: car.series,
                   stock_id: car.stock_id,
                   selling_price: car.selling_price,
                   year: car.year
               }, {});
               focus('focusMe');
           };
//SHOW & HIDE PRICE FILTER
           $scope.priceValue = function (pricesearch) {
               if ($scope.pricesearch == "1") {
                   stateParams.minP = "";
                   stateParams.maxP = "";
                   $scope.selectedMinPrice = "";
                   $scope.selectedMaxPrice = "";
                   $state.go('.', {
                       minP: stateParams.minP,
                       maxP: stateParams.maxP
                   }, {notify: false});
               }
               if ($scope.pricesearch == "0") {
                   stateParams.minPrime = "";
                   stateParams.maxPrime = "";
                   $scope.selectedMinPrime = "";
                   $scope.selectedMaxPrime = "";
                   $state.go('.', {
                       minPrime: stateParams.minPrime,
                       maxPrime: stateParams.maxPrime
                   }, {notify: false});
               }
           };
           $scope.toggleFilter = function (car) {
               car.toggle = !car.toggle;
           };
           showRoom($http, $state);
           showRoom.$inject = [$http, $state, stateParams];

           function showRoom() {
               loadData();
               initLoad();
               vm.detailsClick = function (car) {
                   vm.detailsdata = car;
                   stateParams.car = car;
                   $state.go("cardetails", {
                       stock_id: car.stock_id,
                       series: car.series,
                       make: car.make,
                       year: car.year,
                       selling_price: car.selling_price
                   }, {});
               };
           }

           $scope.changedMake = function () {
               var make = getSelectedMake();
               if (make !== "") {
                   $http.get(dataUrl + "&select=series,year,colour,body_type,year"
                      + (make !== "" ? "&make=eq." + make : "")).then(function (response) {
                       vm.seriesData = [];
                       vm.colourData = [];
                       vm.yearData = [];
                       vm.bodyData = [];
                       for (var i = 0; i < response.data.length; i++) {
                           vm.seriesData.push(response.data[i]);
                           vm.colourData.push(response.data[i]);
                           vm.yearData.push(response.data[i]);
                           vm.bodyData.push(response.data[i]);
                       }
                   });
               }
               else {
                   initLoad()
               }
           };
           $scope.changedSeries = function () {
               var make = getSelectedMake();
               var series = getSelectedSeries();
               if (series !== "") {
                   $http.get(dataUrl + "&select=year,colour,body_type,year"
                      + (make !== "" ? "&make=eq." + make : "")
                      + (series !== "" ? "&series=eq." + series : "")).then(function (response) {
                       vm.colourData = [];
                       vm.yearData = [];
                       vm.bodyData = [];
                       for (var i = 0; i < response.data.length; i++) {
                           vm.colourData.push(response.data[i]);
                           vm.yearData.push(response.data[i]);
                           vm.bodyData.push(response.data[i]);
                       }
                   });
               }
               else {
                   initLoad()
               }
           };
           $scope.changedYear = function () {
               var make = getSelectedMake();
               var series = getSelectedSeries();
               var year = getSelectedYear();
               if (year !== "") {
                   $http.get(dataUrl + "&select=colour,body_type,year"
                      + (make !== "" ? "&make=eq." + make : "")
                      + (series !== "" ? "&series=eq." + series : "")
                      + (year !== "" ? "&year=eq." + year : "")).then(function (response) {
                       vm.colourData = [];
                       vm.bodyData = [];
                       for (var i = 0; i < response.data.length; i++) {
                           vm.colourData.push(response.data[i]);
                           vm.bodyData.push(response.data[i]);
                       }
                   });
               }
               else {
                   initLoad()
               }
           };
           $scope.changedColour = function () {
               var make = getSelectedMake();
               var series = getSelectedSeries();
               var year = getSelectedYear();
               var colour = getSelectedColour();
               if (colour !== "") {
                   $http.get(dataUrl + "&select=body_type,year"
                      + (make !== "" ? "&make=eq." + make : "")
                      + (series !== "" ? "&series=eq." + series : "")
                      + (year !== "" ? "&year=eq." + year : "")
                      + (colour !== "" ? "&colour=eq." + colour : "")).then(function (response) {
                       vm.bodyData = [];
                       for (var i = 0; i < response.data.length; i++) {
                           vm.bodyData.push(response.data[i]);
                       }
                   });
               }
               else {
                   initLoad()
               }
           };
           $scope.changedBody = function () {
               if (getSelectedBody() !== "") {
               }
               else {
                   initLoad()
               }
           };
           $scope.changedMinMile = function () {
               var minMile = getSelectedMinMile();
               if (minMile !== "") {
               }
           };
           $scope.changedMaxMile = function () {
               var maxMile = getSelectedMaxMile();
               if (maxMile !== "") {
               }
           };
           $scope.stateSet = function () {
               var minPrice = $scope.selectedMinPrice;
               var maxPrice = $scope.selectedMaxPrice;
               var minPr = $scope.selectedMinPrime;
               var maxPr = $scope.selectedMaxPrime;
               var maxpTrimmed = parseInt(maxPrice);
               var minpTrimmed = parseInt(minPrice);
               var minPrTrimmed = parseInt(minPr);
               var maxPrTrimmed = parseInt(maxPr);
               if (!isNaN(minpTrimmed) && minpTrimmed !== null && minpTrimmed !== undefined) {
                   stateParams.minP = minPrice;
                   if (maxpTrimmed < minpTrimmed) {
                       $scope.selectedMinPrice = "0";
                       stateParams.minP = $scope.selectedMinPrice;
                   }
               }
               if (!isNaN(maxpTrimmed) && maxpTrimmed !== null && maxpTrimmed !== undefined) {
                   stateParams.maxP = maxPrice;
               }
               if (!isNaN(minPrTrimmed) && minPrTrimmed !== null && minPrTrimmed !== undefined) {
                   stateParams.minPrime = minPr;
                   if (maxPrTrimmed < minPrTrimmed) {
                       $scope.selectedMinPrime = "0";
                       stateParams.minPrime = $scope.selectedMinPrime;
                   }
               }
               if (!isNaN(maxPrTrimmed) && maxPrTrimmed !== null && maxPrTrimmed !== undefined) {
                   stateParams.maxPrime = maxPr;
               }
               stateParams.make = getSelectedMake();
               stateParams.series = getSelectedSeries();
               stateParams.minP = getSelectedMinPrice();
               stateParams.maxP = getSelectedMaxPrice();
               stateParams.minM = getSelectedMinMile();
               stateParams.maxM = getSelectedMaxMile();
               stateParams.year = getSelectedYear();
               stateParams.colour = getSelectedColour();
               stateParams.body_type = getSelectedBody();
               $state.go('.', {
                   make: stateParams.make,
                   series: stateParams.series,
                   minP: stateParams.minP,
                   maxP: stateParams.maxP,
                   minPrime: stateParams.minPrime,
                   maxPrime: stateParams.maxPrime,
                   minM: stateParams.minM,
                   maxM: stateParams.maxM,
                   year: stateParams.year,
                   colour: stateParams.colour,
                   body_type: stateParams.body_type
               }, {notify: false});
               return fetchData();
           };
           $scope.clearFilter = function () {
               stateParams.make = "";
               stateParams.series = "";
               stateParams.minP = "";
               stateParams.maxP = "";
               stateParams.minM = "";
               stateParams.maxM = "";
               stateParams.year = "";
               stateParams.colour = "";
               stateParams.body_type = "";
               stateParams.minPrime = "";
               stateParams.maxPrime = "";
               $state.go('.', {
                   make: stateParams.make,
                   series: stateParams.series,
                   minP: stateParams.minP,
                   maxP: stateParams.maxP,
                   minM: stateParams.minM,
                   maxM: stateParams.maxM,
                   year: stateParams.year,
                   colour: stateParams.colour,
                   body_type: stateParams.body_type,
                   minPrime: stateParams.minPrime,
                   maxPrime: stateParams.maxPrime
               }, {});
           };
           $scope.back = function () {
               window.history.back();
           };
           $scope.scrollToTop = function () {
               if (typeof jQuery == 'undefined') {
                   return window.scrollTo(0, 0);
               }
               else {
                   var body = $('html, body');
                   body.animate({scrollTop: 0}, '600', 'swing');
               }
               return true;
           };
       }
       ])
})();