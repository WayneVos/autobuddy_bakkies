(function () {
    var controllerId = "carEnquire";
    angular.module("app")
       .controller(controllerId, ['$scope', 'configService', '$state', testDrive]);

    function testDrive($scope, configService, $state) {
        $scope.store = configService;
        var vm = this;
        vm.drive = {
            name: "",
            number: "",
            email: "",
            message: "",
            make: "",
            variant: "",
            series: "",
            year: "",
            selling_price: "",
            dealer_email: "",
            dealer_name: "",
            company_id: "",
            guid: "",
            colour: "",
            lead_source: "",
            mileage: "",
            price: "",
            selling: "",
            stock_id: ""
        };

        function spinner() {
            document.getElementById("submitbut2").addEventListener("click", function () {
                angular.element("#spinner").show();
            });
        }

        spinner();
        vm.sendMail = function sendEmail() {
            var driveRequest = "";
            var driveRequestCRM = "";
            var data = {
                notification: {
                    ToList: $scope.store.eMail.carEnquireEmail,
                    CcList: vm.drive.email,// vm.drive.email
                    BccList: $scope.store.eMail.bccEmail,
                    Subject: "We Buy Bakkies Enquiry from " + vm.drive.name + ", with regards to: " + vm.drive.make,
                    Message: vm.drive.message
                    + "\nContact Number: " + vm.drive.number
                    + "\nStock ID: " + vm.drive.stock_id
                    + "\nMake: " + vm.drive.make
                    + "\nVariant: " + vm.drive.variant
                    + "\nYear: " + vm.drive.year
                    + "\nPrice: R" + vm.drive.selling_price,
                    MessageSubject: "We Buy Bakkies Enquiry from " + vm.drive.name,
                    ToName: $scope.store.eMail.carEnquireEmailTo,
                    FromName: vm.drive.name,
                    FromEmail: vm.drive.email
                }
            };
            var dataCrm = {
                "branch_guid": "45482b20-5fe7-4c20-acc4-1fcb6943f5d0",
                "cellphone_no": vm.drive.number.toString(),
                "colour": vm.drive.colour,
                "email_address": vm.drive.email,
                "lead_name": vm.drive.name,
                "lead_source": "We Buy Bakkies Website",
                "make": vm.drive.make,
                "model": vm.drive.series,
                "mileage": Number(vm.drive.mileage),
                "price": Number(vm.drive.selling_price),
                "selling": false,
                "stock_id": Number(vm.drive.stock_id),
                "message": vm.drive.message,
                "year": Number(vm.drive.year)
            };
            driveRequest = JSON.stringify(data);
            driveRequestCRM = JSON.stringify(dataCrm);
            $.ajax({
                url: "https://vmgsoftware.co.za/ShowroomMailServiceV3/MailService.asmx/SendSimpleEmail",
                method: "POST",
                headers: {
                    "content-type": "application/json",
                    accept: "application/json"
                },
                data: driveRequest,
                dataType: "json",
                success: function (response) {
                    document.getElementById("carenquire").reset();
                    angular.element("#carenquire").hide();
                    angular.element("#mailsent").show();
                    angular.element("#spinner").hide();
                },
                error: function (response) {
                    document.getElementById("carenquire").reset();
                    angular.element("#carenquire").hide();
                    angular.element("#mailnotsent").show();
                }
            });
            $.ajax({
                url: "/leads/v1/submit/lead",
                method: "POST",
                headers: {
                    "content-type": "application/json",
                    accept: "application/json",
                    authorization: "Bearer " + SUBMIT_LEAD_TOKEN
                },
                data: driveRequestCRM,
                dataType: "json",
                success: function (response) {
                },
                error: function (response) {
                }
            });
        };
    }
})();