(function () {
    var controllerId = "contactUs";
    angular.module("app").controller(controllerId, ['$scope', 'configService', contactUs]);

    function contactUs($scope, configService) {
        $scope.store = configService;
        var vm = this;
        vm.details = {
            name: "",
            number: "",
            email: "",
            message: ""
        };

        function spinner() {
            document.getElementById("submit").addEventListener("click", function () {
                angular.element("#spinner").css("display", "block");
            });
        }

        spinner();
        vm.sendMail = function sendEmail() {
            var contactRequest = "";
            var data = {
                notification: {
                    ToList: $scope.store.eMail.contactEmail,//$scope.store.eMail.contactEmail
                    CcList: "",
                    BccList: "webmaster@vmgsoftware.co.za",
                    Subject: "Contact request from " + vm.details.name,
                    Message: vm.details.message +
                    "\n\nContact me on: " + vm.details.number,
                    MessageSubject: "Contact request from " + vm.details.name,
                    ToName: $scope.store.eMail.contactEmailTo,
                    FromName: vm.details.name,
                    FromEmail: vm.details.email
                }
            };
            contactRequest = JSON.stringify(data);
            $.ajax({
                url: "http://www.vmgsoftware.co.za/ShowroomMailServiceV3/MailService.asmx/SendSimpleEmail",
                method: "POST",
                headers: {
                    "content-type": "application/json",
                    accept: "application/json"
                },
                data: contactRequest,
                dataType: "json",
                success: function (response) {
                    document.getElementById("contactForm").reset();
                    angular.element("#contactForm").css("display", "none");
                    angular.element("#mailsent").css("display", "block");
                    angular.element("#spinner").css("display", "none");
                },
                error: function (response) {
                    document.getElementById("contactForm").reset();
                    angular.element("#contactForm").css("display", "none");
                    angular.element("#mailnotsent").css("display", "block");
                }
            });
        };
    }
})();