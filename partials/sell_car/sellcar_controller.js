(function () {
    'use strict';
    var controllerId = 'tradeInController';
    angular.module('app').controller(controllerId, ['$scope', '$http', '$state', 'configService', tradeInFunction]);

    function tradeInFunction($scope, $http, $state, configService) {
        $scope.store = configService;
        var vm = this;
        vm.mmData = [];
        vm.makeData = [];
        vm.modelData = [];
        vm.variantData = [];
        vm.details = {
            make: '',
            model: '',
            year: '',
            mileage: '',
            name: '',
            surname: '',
            number: '',
            email: '',
            price: '',
            colour: '',
            region: '',
            comment: ''
        };
        spinner();

        function spinner() {
            document.getElementById("submit1").addEventListener("click", function () {
                angular.element("#spinner").css("display", "block", "width", "50%");
            });
        }

        var dataUrlBasePath = "https://www.bluechipdealers.co.za/api/v3/view_all_vehicle_codes?";
        ShowController($http, $state);

        function ShowController() {
            refreshFilters();
        }

        function getSelectedMakeText() {
            return vm.details.selectedMake.toString() ? vm.details.selectedMake : "";
        }

        function getSelectedSeriesText() {
            return vm.details.selectedSeries.toString() ? vm.details.selectedSeries : "";
        }

        function getSelectedVariantText() {
            return vm.details.selectedVariant.toString() ? vm.details.selectedVariant : "";
        }

        function refreshFilters() {
            $http.get(dataUrlBasePath + "&select=make").then(function (response) {
                vm.makeData = [];
                for (var counter = 0; counter < response.data.length; counter++) {
                    vm.makeData.push(response.data[counter]);
                }
            });
        }

        $scope.selectChangedMake = function () {
            var make = getSelectedMakeText();
            if (make !== "") {
                $http.get(dataUrlBasePath + "&select=model"
                   + (make !== "" ? "&make=eq." + make : "")).then(function (response) {
                    vm.modelData = [];
                    for (var counter = 0; counter < response.data.length; counter++) {
                        vm.modelData.push(response.data[counter]);
                    }
                });
            }
            else {
            }
        };
        $scope.selectChangedSeries = function () {
            var make = getSelectedMakeText();
            var model = getSelectedSeriesText();
            if (model !== "") {
                $http.get(dataUrlBasePath + "&select=variant,mmcode"
                   + (make !== "" ? "&make=eq." + make : "")
                   + (model !== "" ? "&model=eq." + model : "")).then(function (response) {
                    vm.variantData = [];
                    for (var counter = 0; counter < response.data.length; counter++) {
                        vm.variantData.push(response.data[counter]);
                    }
                });
            }
            else {
            }
        };
        $scope.selectChangedVariant = function () {
            var make = getSelectedMakeText();
            var model = getSelectedSeriesText();
            var variant = getSelectedVariantText();
            if (getSelectedVariantText() !== "") {
                $http.get(dataUrlBasePath + "&select=mmcode"
                   + (make !== "" ? "&make=eq." + make : "")
                   + (model !== "" ? "&model=eq." + model : "")
                   + (variant !== "" ? "&variant=eq." + variant : "")).then(function (response) {
                    vm.mData = [];
                    for (var counter = 0; counter < response.data.length; counter++) {
                        vm.mData.push(response.data[counter]);
                    }
                    $scope.mCode = vm.mData[0].mmcode;
                });
            }
            else {
            }
        };
        vm.sendEmail = sendEmail;
        vm.totalFileSize = totalFileSize;

        function buildSimpleMessageWithAttachments(details, files) {
            return {
                ToList: $scope.store.eMail.carSellEmail,
                CcList: "",
                BccList: $scope.store.eMail.bccEmail,
                Subject: "I would like to sell my car.",
                Message: buildTradeInMessageString(details),
                MessageSubject: 'I would lke to sell my vehicle',
                ToName: $scope.store.eMail.carSellEmailTo,
                FromName: details.name + ' ' + details.surname,
                FromEmail: details.email,
                Attachments: files
            };
        }

        function sendEmail() {
            $.each(vm.flow.files, function (index, file) {
                var progress = file.progress();
                if (progress != 1) {
                    alert("Please wait while loading image " + file.name);
                    return;
                }
            });
            var imageObjs = $('#image-files img');
            var imageContents = {};
            imageObjs.each(function (index, image) {
                imageContents[image.alt] = image.src;
            });
            var notification = buildSimpleMessageWithAttachments(vm.details, imageContents);
            var data = "{'notification':" + JSON.stringify(notification) + "}";
            var maxJsonLength = 100485760; // 10 MiB
            if (data.length > maxJsonLength) {
                alert("Your images are too big (more than 10 MiB). Please reduce the number and/or size of the images");
            }
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/ShowroomMailServiceV3/MailService.asmx/SendSimpleEmailWithAttachments",
                data: data,
                dataType: "json",
                success: function (response) {
                    angular.element("#spinner").css("display", "none");
                    var delayredirect = 500;
                    setTimeout(function () {
                        window.location = '/showroom';
                    }, delayredirect);
                },
                error: function (response) {
                }
            });
        }

        function totalFileSize(flow) {
            var files = flow.files;
            var sum = 0;
            $.each(files, function (index, file) {
                sum += file.size;
            });
            return sum;
        }

        function buildTradeInMessageString(details) {
            var newLine = '\n';
            return 'Name: ' + details.name + ' ' + details.surname + newLine
               + 'Phone Number: ' + details.number + newLine
               + 'Email: ' + details.email + newLine + newLine
               + 'mmCode: ' + $scope.mCode + newLine
               + 'Make: ' + details.selectedMake + newLine
               + 'Model: ' + details.selectedSeries + newLine
               + 'Variant: ' + details.selectedVariant + newLine
               + 'Year: ' + details.year + newLine
               + 'Mileage: ' + details.mileage + newLine
               + 'Asking Price: ' + details.price + newLine
               + 'Colour: ' + details.colour + newLine
               + 'Region: ' + details.region + newLine
               + 'Comments: ' + details.comment;
        }
    }
})();