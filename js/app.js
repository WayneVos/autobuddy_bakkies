(function () {
    "use strict";
    angular.module('app', [
           "ngResource",
           "angular-flexslider",
           "angularValidator",
           "ngSanitize",
           "ui.router",
           "updateMeta",
           "flow",
           "angularUtils.directives.dirPagination",
           "angular.filter",
           "angularLazyLoad",
           "angular-img-lazy-load",
           "ngParallax",
           "ngAnimate",
           "ng-currency"
       ])
       .controller("main_ctr", [function () {
       }
       ])
       .run(function ($rootScope, $state, $document, $stateParams) {
           $rootScope.$state = $state;
           $rootScope.$stateParams = $stateParams;
           $rootScope.$on('$stateChangeSuccess', function () {
               $document[0].body.scrollTop = $document[0].documentElement.scrollTop = 0;
           });
       });
    $(document).on("click", ".navbar-collapse.in", function (e) {
        if ($(e.target).is("a")) {
            $(this).collapse("hide");
        }
    });
    $('ul.nav li.dropdown').hover(function () {
        $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
    }, function () {
        $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
    });
})();