(function () {
    var app = angular.module("app");
    app.config(function ($stateProvider, $urlRouterProvider, $urlMatcherFactoryProvider, $locationProvider) {
        var vehicleType = {
            encode: function (str) {
                return str && str.replace(/ /g, "-");
            },
            decode: function (str) {
                return str && str.replace(/-/g, " ");
            },
            is: angular.isString,
            pattern: /[^/]+/
        };
        $urlMatcherFactoryProvider.type('vehicle', vehicleType);
        $stateProvider
           .state("/", {
               url: "/",
               views: {
                   "": {templateUrl: "partials/home/home.html"},
                   "header@": {templateUrl: "templates/header.html"},
                   "footer@": {templateUrl: "templates/footer.html"},
                   "homeslider@/": {templateUrl: "templates/home_slider.html"}
               },
               controller: "PageCtrl"
           })
           .state("home", {
               url: "/home",
               views: {
                   "": {templateUrl: "partials/home/home.html"},
                   "header@": {templateUrl: "templates/header.html"},
                   "footer@": {templateUrl: "templates/footer.html"},
                   "sell@home": {templateUrl: "partials/sell_car/sell_car.html"},
                   "homeslider@home": {templateUrl: "templates/home_slider.html"}
               },
               controller: "PageCtrl"
           })
           .state("new_vehicles", {
               url: "/new_vehicles",
               views: {
                   "": {templateUrl: "partials/vehicles/new.html"},
                   "header": {templateUrl: "templates/header.html"},
                   "footer": {templateUrl: "templates/footer.html"}
               },
               controller: "PageCtrl"
           })
           .state("sell", {
               url: "/sell",
               views: {
                   "": {
                       templateUrl: "partials/sell_car/sell_car.html",
                       controller: "PageCtrl"
                   },
                   "header": {templateUrl: "templates/header.html"},
                   "footer": {templateUrl: "templates/footer.html"}
               }
           })
           .state("finance", {
               url: "/finance",
               views: {
                   "": {templateUrl: "partials/finance/finance.html"},
                   "header": {templateUrl: "templates/header.html"},
                   "footer": {templateUrl: "templates/footer.html"}
               },
               controller: "PageCtrl"
           })
           .state("about", {
               url: "/about",
               views: {
                   "": {templateUrl: "partials/about/about.html"},
                   "header": {templateUrl: "templates/header.html"},
                   "footer": {templateUrl: "templates/footer.html"}
               },
               controller: "PageCtrl"
           })
           .state("service", {
               url: "/owners-service",
               views: {
                   "": {templateUrl: "partials/service/service.html"},
                   "header": {templateUrl: "templates/header.html"},
                   "footer": {templateUrl: "templates/footer.html"}
               },
               controller: "PageCtrl"
           })
           .state("contact", {
               url: "/contact",
               views: {
                   "": {templateUrl: "partials/contact/contact.html"},
                   "header": {templateUrl: "templates/header.html"},
                   "footer": {templateUrl: "templates/footer.html"}
               },
               controller: "PageCtrl"
           })
           .state("bookservice", {
               url: "/book-service",
               views: {
                   "": {templateUrl: "partials/service/book_service.html"},
                   "header": {templateUrl: "templates/header.html"},
                   "footer": {templateUrl: "templates/footer.html"}
               },
               controller: "PageCtrl"
           })
           .state("testimonials", {
               url: "/testimonials",
               views: {
                   "": {templateUrl: "partials/testimonials/testimonials.html"},
                   "header": {templateUrl: "templates/header.html"},
                   "footer": {templateUrl: "templates/footer.html"}
               },
               controller: "PageCtrl"
           })
           .state("vehicles", {
               url: "/showroom?make&series&minP&maxP&minPrime&maxPrime&minM&maxM&year&colour&body_type",
               param: {
                   car: null,
                   make: null,
                   series: null,
                   minP: null,
                   maxP: null,
                   minPrime: null,
                   maxPrime: null,
                   minM: null,
                   maxM: null,
                   year: null,
                   colour: null,
                   body_type: null
               },
               resolve: {
                   car: function ($stateParams) {
                       return {
                           car: $stateParams.car,
                           make: $stateParams.make,
                           year: $stateParams.year,
                           series: $stateParams.series,
                           minP: $stateParams.minP,
                           maxP: $stateParams.maxP,
                           minPrime: $stateParams.minPrime,
                           maxPrime: $stateParams.maxPrime,
                           minM: $stateParams.minM,
                           maxM: $stateParams.maxM,
                           body_type: $stateParams.body_type,
                           colour: $stateParams.colour
                       }
                   }
               },
               views: {
                   "": {templateUrl: "partials/vehicles/vehicles.html"},
                   "header": {templateUrl: "templates/header.html"},
                   "footer": {templateUrl: "templates/footer.html"},
                   "homeslider@home": {templateUrl: "templates/home_slider.html"}
               },
               controller: "PageCtrl"
           })
           .state("cardetails", {
               url: "/used-cars/:stock_id/:make/:series/:year/:selling_price",
               params: {
                   car: null,
                   stock_id: null,
                   year: null,
                   make: null,
                   series: null,
                   selling_price: null
               },
               resolve: {
                   car: function ($stateParams) {
                       return {
                           car: $stateParams.car,
                           stock_id: $stateParams.stock_id,
                           make: $stateParams.make,
                           year: $stateParams.year,
                           series: $stateParams.series,
                           selling_price: $stateParams.selling_price
                       }
                   }
               },
               views: {
                   "": {
                       templateUrl: "partials/vehicles/cardetails.html",
                       controller: "Details"
                   },
                   "header@": {templateUrl: "templates/header.html"},
                   "footer@": {templateUrl: "templates/footer.html"}
               }
           })
           .state("404", {
               url: "/404",
               templateUrl: "partials/404.html",
               controller: "PageCtrl"
           })
           .state("otherwise", {
               url: "/404",
               templateUrl: "partials/404.html",
               controller: "PageCtrl"
           });
        $urlRouterProvider.when("", "/home");
        $urlRouterProvider.otherwise(function ($injector, $location) {
            $injector.invoke(['$state', function ($state) {
                $state.go('404');
            }
            ]);
            return true;
        });
        $locationProvider.html5Mode(true);
    });
    app.controller("PageCtrl", function () {
    });
    var dataUrlBasePath = "http://autoam.vmgtest.co.za/api/v3/view_stock_complete_with_data?company_id=eq.571&select=*";
    app.controller('Details', ["$scope", "$http", "$stateParams", function ($scope, $http, $stateParams) {
        function detailsFetch() {
            $http.get(dataUrlBasePath
               + "&stock_id=eq." + $stateParams.stock_id
               + "&selling_price=eq." + $stateParams.selling_price).then(function (response) {
                $scope.car = response.data[0];
                $scope.slider = [
                    $scope.car.url1,
                    $scope.car.url2,
                    $scope.car.url3,
                    $scope.car.url4,
                    $scope.car.url5,
                    $scope.car.url6,
                    $scope.car.url7,
                    $scope.car.url8,
                    $scope.car.url9,
                    $scope.car.url10,
                    $scope.car.url11,
                    $scope.car.url12,
                    $scope.car.url13,
                    $scope.car.url14,
                    $scope.car.url15,
                    $scope.car.url16,
                    $scope.car.url17,
                    $scope.car.url18,
                    $scope.car.url19,
                    $scope.car.url20
                ];
            });
        }

        detailsFetch(
           $stateParams.stock_id,
           $stateParams.selling_price);
        $scope.cancel = function () {
            window.history.back();
        };
    }
    ]);
})();