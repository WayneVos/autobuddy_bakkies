(function () {
    var app = angular.module("app");
    app.factory('configService', function () {
        var store = {
            name: "We Buy Bakkies",
            branch1: "We Buy Bakkies",
            email1: "info@webuybakkies.co.za",
            facebookUrl: "",
            instaUrl: " ",
            twitterUrl: "",
            googleplusUrl: " ",
            youtubeUrl: "",
            address1: "81 Sterling Road, Samrand, Midrand",
            tel1: "060 911 2233",
            tel2: "+27 (0) 12 657 0234",
            tel3: "+27 (0) 82 657 7777",
            cell1: "081 522 1752",
            fax1: "+27 (0) 12 657 0237",
            eMail: {
                contactEmail: "info@webuybakkies.co.za",
                contactEmailTo: "We Buy Bakkies",
                carRequestEmail: "info@webuybakkies.co.za",
                carRequestEmailTo: "We Buy Bakkies",
                carSellEmail: "info@webuybakkies.co.za",
                carSellEmailTo: "We Buy Bakkies",
                carServiceEmail: "info@webuybakkies.co.za",
                carServiceEmailTo: "We Buy Bakkies",
                carFinanceEmail: "info@webuybakkies.co.za",
                carFinanceEmailTo: "We Buy Bakkies",
                carEnquireEmail: "info@webuybakkies.co.za",
                carEnquireEmailTo: "We Buy Bakkies",
                testimonialEmail: "info@webuybakkies.co.za",
                testimonialEmailTo: "We Buy Bakkies",
                bccEmail: "webmaster@vmgsoftware.co.za",
                ccEmail: ""
            },
            tradinghours: {
                monFri: '09:00-18:00',
                sat: '09:00 – 13:00',
                sun: 'Closed',
                pub: 'Closed'
            },
            latlong: '-29.723298, 31.063959',
            latlong1: '',
            mapLink: "https://www.google.co.za/maps/place/6+Jubilee+Grove,+Umhlanga+Ridge,+Umhlanga,+4319/@-29.723298,31.0617703,17z/data=!3m1!4b1!4m5!3m4!1s0x1ef7059339f64ed9:0xd4b089658d10dd25!8m2!3d-29.723298!4d31.063959",
            mapLinkbranch1: "https://www.google.co.za/maps/place/6+Jubilee+Grove,+Umhlanga+Ridge,+Umhlanga,+4319/@-29.723298,31.0617703,17z/data=!3m1!4b1!4m5!3m4!1s0x1ef7059339f64ed9:0xd4b089658d10dd25!8m2!3d-29.723298!4d31.063959",
            mapLinkShortLink1: ""
        };
        return store;
    });
})();