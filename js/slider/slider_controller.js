(function () {
    angular.module("app")
       .controller("slider", ["$scope", function ($scope) {
           $scope.slides = [
               {
                   url: "img/slides/s1a.jpg",
                   text1: "QUALITY CARS",
                   text2: "TOP QUALITY CARS, TOP QUALITY SERVICE!",
                   text4: "Our aim is to supply top quality cars, top quality service and the best value money can buy for each specific and individualised transaction."
               },
               {
                   url: "img/slides/s3a.jpg",
                   text1: "PROFESSIONAL and EFFICIENT",
                   text2: "",
                   text4: "Our professional sales team is trained to assess our customer’s needs, provide recommendations where required and facilitate the purchase of a vehicle as quickly and efficiently as possible. "
               },
               {
                   url: "img/slides/s2a.jpg",
                   text1: "EXCEPTIONAL",
                   text3: "...exceptional pre-owned vehicles",
                   text4: "Our goal is to provide exceptional pre-owned vehicles, each thoroughly inspected and test driven to ensure that only the highest standard of quality vehicles end up on our impressive showroom floor."
               }
           ];
           $scope.sliderOptions = {
               controlNav: false,
               animationLoop: true,
           }
       }
       ])
})();